/**
 * TodoController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    create: async (req, res) => {
        try {
            let tempTodo = req.body;
            let createdTodo = await Todo.create(tempTodo).fetch();
            return res.ok(createdTodo);
        } catch (error) {
            return res.serverError(error);
        }
    },

    findAll: async (req, res) => {
        try {
            let listOfTodos = await Todo.find({});
            console.table(listOfTodos);
            return res.ok(listOfTodos);
        } catch (error) {
            return res.serverError(error);
        }
    },

    findOne: async (req, res) => {
        try {
            let id = req.param('id');
            let record = await Todo.find({ _id: id });
            return res.ok(record);
        } catch (error) {
            return res.serverError(error);
        }
    },

    update: async (req, res) => {
        try {
            let id = req.param('id');
            let body = req.body;
            return res.ok(await Todo.updateOne({ _id: id }).set(body));
        } catch (error) {
            return res.serverError(error);
        }
    },

    delete: async (req, res) => {
        try {
            let id = req.param('id');
            let result = await Todo.destroy({ _id: id }).fetch();
            return res.ok(result);
        } catch (error) {
            return res.serverError(error);
        }
    }
};

