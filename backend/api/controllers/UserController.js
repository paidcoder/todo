/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');

module.exports = {

    signup: async (req, res) => {
        try {
            let email = req.body.email;
            let username = req.body.username;
            let password = req.body.password;

            // Check if all the fields are provided
            if (!email || !username || !password) {
                return res.json(401, { err: 'Please fill in all details' });
            }

            // Check if the email is already in use.
            let existingUser = await User.findOne({email: req.body.email});
            if (existingUser) {
                return res.serverError("Email Id is already in use!");
            }

            let userInfo = { username: req.body.username, email: req.body.email, password: req.body.password };
            let user = await User.create(userInfo).fetch();
            if (user) {
                res.ok("User created successfully");
            }

        } catch (error) {
            return res.serverError(error);
        }
    },

    login: async (req, res) => {
        try {
            let email = req.body.email;
            let password = req.body.password;
            let user = {
                sub: 'User details',
                email: email
            }

            let token = jwt.sign(user, sails.config.session.secret, { expiresIn: "24h" });

            if (!email || !password) {
                return res.json(401, { err: "Email and Password required" });
            }

            let foundUser = await User.findOne({ email: req.body.email });
            if (!foundUser) {
                return res.serverError("User with given email id not found!");
            }

            bcrypt.compare(req.body.password, foundUser.password, (err, result) => {
                console.log(result);
                if (err) {
                    return res.serverError(err);
                }
                if (result) {
                    return res.json({ success: true, token: token, user: foundUser.username });
                } else {
                    return res.json(401, { err: 'invalid username or password' });
                }
            });
        } catch (error) {
            return res.serverError(error);
        }
    }


};

