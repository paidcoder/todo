/**
 * Todo.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    title: {
      type: 'string',
      required: true
    },

    description: {
      type: 'string'
    },

    assignedTo: {
      type: 'string'
    },

    priority: {
      type: 'string',
      isIn: ['low', 'meduim', 'high'],
      defaultsTo: 'medium'
    },

    status: {
      type: 'string',
      isIn: ['created', 'completed', "inProgress", "waiting", "cancelled"],
      defaultsTo: 'created'
    },

    effortNeeded: {
      type: 'number',
      required: true
    },

    sprint: {
      type: 'string',
      required: true
    },

    type: {
      type: 'string'
    },

    tags: {
      type: 'json',
      columnType: 'array'
    }

  },

};

