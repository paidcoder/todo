/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

var bcrypt = require('bcrypt');

module.exports = {

  attributes: {

    username: {
      type: 'string',
      required: true
    },

    email: {
      type: 'string',
      isEmail: true,
      required: true,
      unique: true
    },

    password: {
      type: 'string',
      required: true
    }
  },

  beforeCreate: (values, next) => {
    bcrypt.hash(values.password, 10, (err, hash) => {
      if (err) {
        return next(err);
      }
      values.password = hash;
      next();
    });
  }

};

