import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './common/page-not-found/page-not-found.component';
var routes = [{
        path: '', component: LoginComponent
    }, {
        path: 'todos', loadChildren: './todo/todo.module#TodoModule'
    }, {
        path: 'login', redirectTo: '', pathMatch: 'full'
    }, {
        path: '**', component: PageNotFoundComponent
    }];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [RouterModule.forRoot(routes)],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map