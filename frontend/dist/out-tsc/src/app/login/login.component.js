import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from "@angular/router";
var LoginComponent = /** @class */ (function () {
    function LoginComponent(router) {
        this.router = router;
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        $('#loginForm')
            .form({
            fields: {
                userName: ['empty', 'email'],
                password: ['minLength[6]', 'empty'],
            },
            onSuccess: function () {
                _this.onSubmit($('#loginForm')
                    .form('get values'));
            }
        });
    };
    LoginComponent.prototype.onSubmit = function (userCreds) {
        console.log('creds ' + userCreds.userName);
        this.router.navigate(['todos']);
    };
    LoginComponent = tslib_1.__decorate([
        Component({
            selector: 'app-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [Router])
    ], LoginComponent);
    return LoginComponent;
}());
export { LoginComponent };
//# sourceMappingURL=login.component.js.map