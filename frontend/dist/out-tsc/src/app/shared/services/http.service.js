import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
var HttpService = /** @class */ (function () {
    function HttpService(http) {
        this.http = http;
        this.baseUrl = 'http://54.164.72.34:1337';
    }
    HttpService.prototype.getAll = function (endPoint) {
        return this.http.get(this.baseUrl + "/" + endPoint);
    };
    HttpService.prototype.getOne = function (endPoint, id) {
        return this.http.get(this.baseUrl + "/" + endPoint + "/" + id);
    };
    HttpService.prototype.post = function (endPoint, data) {
        return this.http.post(this.baseUrl + "/" + endPoint, data);
    };
    HttpService.prototype.update = function (endPoint, data, id) {
        return this.http.put(this.baseUrl + "/" + endPoint + "/" + id, data);
    };
    HttpService.prototype.delete = function (endPoint, id) {
        return this.http.delete(this.baseUrl + "/" + endPoint + "/" + id);
    };
    HttpService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], HttpService);
    return HttpService;
}());
export { HttpService };
//# sourceMappingURL=http.service.js.map