import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { TodoService } from '../todo.service';
import { Router } from "@angular/router";
var CreateTodoComponent = /** @class */ (function () {
    function CreateTodoComponent(todoService, router) {
        this.todoService = todoService;
        this.router = router;
    }
    CreateTodoComponent.prototype.ngOnInit = function () {
        var _this = this;
        $('#taskForm')
            .form({
            fields: {
                title: ['empty'],
                sprint: ['empty'],
                effortNeeded: ['empty'],
            },
            onSuccess: function () {
                console.log('successfull');
                _this.onSubmit($('#taskForm')
                    .form('get values'));
            }
        });
    };
    CreateTodoComponent.prototype.onSubmit = function (task) {
        console.log(task);
        this.todoService.createTask('todos', task);
    };
    CreateTodoComponent = tslib_1.__decorate([
        Component({
            selector: 'app-create-todo',
            templateUrl: './create-todo.component.html',
            styleUrls: ['./create-todo.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [TodoService, Router])
    ], CreateTodoComponent);
    return CreateTodoComponent;
}());
export { CreateTodoComponent };
//# sourceMappingURL=create-todo.component.js.map