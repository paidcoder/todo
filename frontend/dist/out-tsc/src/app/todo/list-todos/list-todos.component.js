import * as tslib_1 from "tslib";
import { Component, Inject } from '@angular/core';
import { TodoService } from '../todo.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
var ListTodosComponent = /** @class */ (function () {
    function ListTodosComponent(todoService, dialog) {
        var _this = this;
        this.todoService = todoService;
        this.dialog = dialog;
        this.tasks = [];
        this.user = [];
        this.todoService.getTodos().subscribe(function (data) {
            _this.tasks = data;
        });
    }
    ListTodosComponent.prototype.ngOnInit = function () {
        //Dummy data for assigning developers
        this.user = [{
                name: 'Nawazulla Shariff',
                image: '../assets/GitHub-Mark-32px.png',
                role: 'FrontEnd Developer'
            },
            {
                name: 'Vinayak Shivpuje',
                image: '../assets/GitHub-Mark-32px.png',
                role: 'Full Stack Developer'
            },
            {
                name: 'Yeshwanth Naidu',
                image: '../assets/GitHub-Mark-32px.png',
                role: 'FrontEnd Developer'
            },];
    };
    ListTodosComponent.prototype.onDelete = function (id) {
        console.log(id);
        this.todoService.deleteTodo('todos', id);
    };
    ListTodosComponent.prototype.assignTask = function () {
        console.log('came here');
        var dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
            width: '250px',
            data: { user: this.user }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log('The dialog was closed');
        });
    };
    ListTodosComponent = tslib_1.__decorate([
        Component({
            selector: 'app-list-todos',
            templateUrl: './list-todos.component.html',
            styleUrls: ['./list-todos.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [TodoService, MatDialog])
    ], ListTodosComponent);
    return ListTodosComponent;
}());
export { ListTodosComponent };
var DialogOverviewExampleDialog = /** @class */ (function () {
    function DialogOverviewExampleDialog(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    DialogOverviewExampleDialog.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    DialogOverviewExampleDialog.prototype.assignUser = function (user) {
        console.log(user);
        //update task endpoint should be called here
    };
    DialogOverviewExampleDialog = tslib_1.__decorate([
        Component({
            selector: 'dialog-overview-example-dialog',
            templateUrl: 'dialog-overview-example-dialog.html',
        }),
        tslib_1.__param(1, Inject(MAT_DIALOG_DATA)),
        tslib_1.__metadata("design:paramtypes", [MatDialogRef, Object])
    ], DialogOverviewExampleDialog);
    return DialogOverviewExampleDialog;
}());
export { DialogOverviewExampleDialog };
//# sourceMappingURL=list-todos.component.js.map