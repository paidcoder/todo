import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
var ShowTodoComponent = /** @class */ (function () {
    function ShowTodoComponent() {
    }
    ShowTodoComponent.prototype.ngOnInit = function () {
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], ShowTodoComponent.prototype, "task", void 0);
    ShowTodoComponent = tslib_1.__decorate([
        Component({
            selector: 'app-show-todo',
            templateUrl: './show-todo.component.html',
            styleUrls: ['./show-todo.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], ShowTodoComponent);
    return ShowTodoComponent;
}());
export { ShowTodoComponent };
//# sourceMappingURL=show-todo.component.js.map