import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var TodoLayoutComponent = /** @class */ (function () {
    function TodoLayoutComponent() {
    }
    TodoLayoutComponent.prototype.ngOnInit = function () {
    };
    TodoLayoutComponent = tslib_1.__decorate([
        Component({
            selector: 'app-todo-layout',
            templateUrl: './todo-layout.component.html',
            styleUrls: ['./todo-layout.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], TodoLayoutComponent);
    return TodoLayoutComponent;
}());
export { TodoLayoutComponent };
//# sourceMappingURL=todo-layout.component.js.map