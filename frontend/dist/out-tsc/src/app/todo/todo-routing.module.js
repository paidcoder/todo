import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TodoLayoutComponent } from './todo-layout/todo-layout.component';
var routes = [{
        path: '', component: TodoLayoutComponent
    }];
var TodoRoutingModule = /** @class */ (function () {
    function TodoRoutingModule() {
    }
    TodoRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [RouterModule.forChild(routes)],
            exports: [RouterModule]
        })
    ], TodoRoutingModule);
    return TodoRoutingModule;
}());
export { TodoRoutingModule };
//# sourceMappingURL=todo-routing.module.js.map