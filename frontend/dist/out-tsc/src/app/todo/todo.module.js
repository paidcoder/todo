import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoRoutingModule } from './todo-routing.module';
import { ListTodosComponent } from './list-todos/list-todos.component';
import { CreateTodoComponent } from './create-todo/create-todo.component';
import { TodoLayoutComponent } from './todo-layout/todo-layout.component';
import { TodoService } from './todo.service';
import { ShowTodoComponent } from './show-todo/show-todo.component';
import { MaterialModule } from '../shared/material/material.module';
var TodoModule = /** @class */ (function () {
    function TodoModule() {
    }
    TodoModule = tslib_1.__decorate([
        NgModule({
            declarations: [ListTodosComponent, CreateTodoComponent, TodoLayoutComponent, ShowTodoComponent],
            imports: [
                CommonModule,
                TodoRoutingModule,
                // BrowserAnimationsModule,
                MaterialModule
            ],
            providers: [TodoService]
        })
    ], TodoModule);
    return TodoModule;
}());
export { TodoModule };
//# sourceMappingURL=todo.module.js.map