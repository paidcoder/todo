import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpService } from '../shared/services/http.service';
var TodoService = /** @class */ (function () {
    function TodoService(httpService) {
        this.httpService = httpService;
    }
    TodoService.prototype.getTodos = function () {
        return this.httpService.getAll('todo');
    };
    TodoService.prototype.createTask = function (endpoint, task) {
        this.httpService.post(endpoint, task).subscribe(function (id) {
            console.log(id);
        });
    };
    TodoService.prototype.getTodo = function (id) {
    };
    TodoService.prototype.deleteTodo = function (endpoint, id) {
        this.httpService.delete(endpoint, id).subscribe(function (id) {
            console.log(id);
        });
    };
    TodoService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [HttpService])
    ], TodoService);
    return TodoService;
}());
export { TodoService };
//# sourceMappingURL=todo.service.js.map