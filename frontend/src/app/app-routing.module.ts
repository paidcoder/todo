import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './common/page-not-found/page-not-found.component';

const routes: Routes = [{
  path: '', component: LoginComponent
}, {
  path: 'todos', loadChildren: './todo/todo.module#TodoModule'
}, {
  path: 'login', redirectTo: '', pathMatch: 'full'
}, {
  path: '**', component: PageNotFoundComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
