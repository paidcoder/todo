import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

declare var $: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    $('#loginForm')
      .form({
        fields: {
          userName: ['empty','email'],
          password: ['minLength[6]', 'empty'],
        },
        onSuccess: () => {

          this.onSubmit($('#loginForm')
            .form('get values'));
        }
      });
  }

  onSubmit(userCreds: any) {
console.log('creds '+userCreds.userName);
this.router.navigate(['todos']);
  }
}
