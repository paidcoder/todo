export interface Task {
    title: string;
    description?: string;
    assignedTo?: string;
    priority: string;
    effortNeeded: number; // in hours
    sprint: string; // yymmdd
    type?: string;
    tags?: string[];
    status: string;
}