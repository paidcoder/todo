export interface User {
    name: string;
    image: any;
    role: string;
  }