import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  
  baseUrl = 'http://54.164.72.34:1337';

  constructor(private http: HttpClient) { }

  getAll(endPoint) {
    return this.http.get(`${this.baseUrl}/${endPoint}`);
  }

  getOne(endPoint, id) {
    return this.http.get(`${this.baseUrl}/${endPoint}/${id}`);
  }

  post(endPoint, data) {
    return this.http.post(`${this.baseUrl}/${endPoint}`, data);
  }

  update(endPoint, data, id) {
    return this.http.put(`${this.baseUrl}/${endPoint}/${id}`, data)
  }

  delete(endPoint, id) {
    return this.http.delete(`${this.baseUrl}/${endPoint}/${id}`);
  }
}
