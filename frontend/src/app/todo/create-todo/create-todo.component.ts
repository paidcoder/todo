import { Component, OnInit } from '@angular/core';
import { TodoService } from '../todo.service';
import { Router } from "@angular/router";
declare var $: any;
@Component({
  selector: 'app-create-todo',
  templateUrl: './create-todo.component.html',
  styleUrls: ['./create-todo.component.scss']
})
export class CreateTodoComponent implements OnInit {

  constructor(private todoService: TodoService, private router: Router) { }

  ngOnInit() {
    $('#taskForm')
      .form({
        fields: {
          title: ['empty'],
          sprint: ['empty'],
          effortNeeded:['empty'],

        },
        onSuccess: () => {
          console.log('successfull');
          this.onSubmit($('#taskForm')
            .form('get values'));
        }
      });
  }

  onSubmit(task: any) {
    console.log(task);
    this.todoService.createTask('todos',task);
    
  }

}
