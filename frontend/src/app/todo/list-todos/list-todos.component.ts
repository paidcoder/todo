import { Component, OnInit, Inject } from '@angular/core';
import { TodoService } from '../todo.service';
import { Task } from 'src/app/shared/models/task.model';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { User } from 'src/app/shared/models/user.model';
declare var $: any;






@Component({
  selector: 'app-list-todos',
  templateUrl: './list-todos.component.html',
  styleUrls: ['./list-todos.component.scss']
})
export class ListTodosComponent implements OnInit {

  tasks: Task[] = [];
  name: string;
  user: User[] = [];




  constructor(private todoService: TodoService, public dialog: MatDialog) {

    this.todoService.getTodos().subscribe((data: Task[]) => {
      this.tasks = data;
    });
  }

  ngOnInit() {
    //Dummy data for assigning developers
    this.user = [{
      name: 'Nawazulla Shariff',
      image: '../assets/GitHub-Mark-32px.png',
      role: 'FrontEnd Developer'
    },
    {
      name: 'Vinayak Shivpuje',
      image: '../assets/GitHub-Mark-32px.png',
      role: 'Full Stack Developer'
    },
    {
      name: 'Yeshwanth Naidu',
      image: '../assets/GitHub-Mark-32px.png',
      role: 'FrontEnd Developer'
    },];
  }

  onDelete(id: any) {
    console.log(id);
    this.todoService.deleteTodo('todos', id);
  }

  assignTask(id:any) {
    console.log('came here');
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '250px',
      data: { user: this.user ,taskId: id}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

    });
  }


}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class DialogOverviewExampleDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: User) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  assignUser(user: User,id:any){
    console.log(user+' '+id);
    //update task endpoint should be called here

  }

}


