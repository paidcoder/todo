import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations'
import { TodoRoutingModule } from './todo-routing.module';
import { ListTodosComponent } from './list-todos/list-todos.component';
import { CreateTodoComponent } from './create-todo/create-todo.component';
import { TodoLayoutComponent } from './todo-layout/todo-layout.component';
import { TodoService } from './todo.service';
import { ShowTodoComponent } from './show-todo/show-todo.component';
import { MaterialModule } from '../shared/material/material.module';

@NgModule({
  declarations: [ListTodosComponent, CreateTodoComponent, TodoLayoutComponent, ShowTodoComponent],
  imports: [
    CommonModule,
    TodoRoutingModule,
    // BrowserAnimationsModule,
    MaterialModule
  ],
  providers:[TodoService]
})
export class TodoModule { }
