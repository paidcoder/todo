import { Injectable } from '@angular/core';
import { HttpService } from '../shared/services/http.service';
import { Observable } from 'rxjs';
import { Task } from 'src/app/shared/models/task.model';

@Injectable()
export class TodoService {

  constructor(private httpService: HttpService) { }

  getTodos() {
    return this.httpService.getAll('todo');
  }

  createTask(endpoint, task) {
    this.httpService.post(endpoint, task).subscribe(id => {
      console.log(id);
    });
  }

  getTodo(id) {

  }

  deleteTodo(endpoint, id){
    this.httpService.delete(endpoint,id).subscribe(id=>{
      console.log(id);
    })
  }
}
